var animateHTML = function () {
    var elems,elems2SecDelay,elems4SecDelay,elems6SecDelay,windowHeight,elems2sec
    var init = function () {
      elems = document.getElementsByClassName('animateresponsivehidden');
      elems2SecDelay = document.getElementsByClassName('animateresponsivehidden2sec');
      elems4SecDelay = document.getElementsByClassName('animateresponsivehidden4sec');
      elems6SecDelay = document.getElementsByClassName('animateresponsivehidden6sec');
      elems2sec=document.getElementsByClassName('animateContent');
      windowHeight = window.innerHeight
      _addEventHandlers()
    }
    var _addEventHandlers = function () {
      window.addEventListener('scroll', _checkPosition)
      window.addEventListener('resize', init)
    }
    var _checkPosition = function () {
       for (var i = 0; i < elems.length; i++) {
        var posFromTop = elems[i].getBoundingClientRect().top
        if (posFromTop - windowHeight <= 0) {
          elems[i].className = elems[i].className.replace('animateresponsivehidden', 'animateresponsive')
        }
        }
       for (var i = 0; i < elems2SecDelay.length; i++) {
        var posFromTop = elems2SecDelay[i].getBoundingClientRect().top
        if (posFromTop - windowHeight <= 0) {
            elems2SecDelay[i].className = elems2SecDelay[i].className.replace('animateresponsivehidden2sec', 'animateresponsiveDelayed2Sec')
        }
        }

        for (var i = 0; i < elems4SecDelay.length; i++) {
        var posFromTop = elems4SecDelay[i].getBoundingClientRect().top
        if (posFromTop - windowHeight <= 0) {
            elems4SecDelay[i].className = elems4SecDelay[i].className.replace('animateresponsivehidden4sec', 'animateresponsiveDelayed4Sec')
        }
        }

        for (var i = 0; i < elems6SecDelay.length; i++) {
        var posFromTop = elems6SecDelay[i].getBoundingClientRect().top
        if (posFromTop - windowHeight <= 0) {
            elems6SecDelay[i].className = elems6SecDelay[i].className.replace('animateresponsivehidden6sec', 'animateresponsiveDelayed6Sec')
        }
        }
        for (var i = 0; i < elems2sec.length; i++) {
          var posFromTop = elems2sec[i].getBoundingClientRect().top
          if (posFromTop - windowHeight <= 0) {
            elems2sec[i].className = elems2sec[i].className.replace('animateContent','animateContentResponsive')
        }
        }
    }

    return {
      initate: init
    }
  }
  animateHTML().initate()